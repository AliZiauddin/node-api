const {check, validationResult} = require('express-validator');

exports.createPostValidator = () => [
    //title
    check('title', "Title is required").notEmpty(),
    check("title", "Title  must be between 4 to 250 characters").isLength({
        min:4,
        max:150
    }),

    
    //body
    check('body', "Body is required").notEmpty(),
    check("body", "body  must be between 4 to 2000 characters").isLength({
        min:4,
        max:2000
    }),

    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array()[0]['msg'] });
          }
        
        // res.send('no error');
        next();
    }

]

exports.userSignupValidator = () => [
    // name is not null and  between 4 - 10 characters
    check('name', 'Name is required').notEmpty(),

    //email is not null, valid and normalized
    check('email', 'Email must be between 3 and 32 characters')
    .matches(/.+\@.+\..+/)
    .withMessage('Email must contail @')
    .isLength({
        min: 4,
        max: 2000
    }),

    // check for password
    check('password', 'Password is required').notEmpty(),
    check('password')
    .isLength({min: 6})
    .withMessage("Password must contain atleast 6 characters")
    .matches(/\d/) 
    .withMessage('Password must contain a digit'),
    
    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array()[0]['msg'] });
          }
        
        // res.send('no error');
        next();
    }
    
]