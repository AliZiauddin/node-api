const express = require('express');
const app = express();
const mongoose = require('mongoose');
const morgan = require('morgan');
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
const cookiepParser = require('cookie-parser');
// const expressValidator = require('express-validator');
dotenv.config()

//
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);

mongoose.connect(process.env.MONGO_URI)
.then(() => console.log('DB connected'))

mongoose.connection.on('error', err => {
    console.log(`DB connection error ${err.message}`);
})

// bring routes
const postRoutes = require('./routes/post');
const authRoutes = require('./routes/auth');

// middleware

app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(cookiepParser());
app.use("/", postRoutes);
app.use("/", authRoutes);


const port = process.env.PORT || 3000;
app.listen(port, () => {console.log(`Node js API is listening on port: ${port}`)});
